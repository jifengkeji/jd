## 环境实拍图

## 00公司执照
![PNG](src/00.png)  

## 01大楼外观  
![PNG](src/01大楼外观.png)  


## 02办公室    
![PNG](src/02办公室.png)  

## 03周边环境    
![PNG](src/03周边环境.png)  

![PNG](src/04周边环境2.png)  

## 04生活超市    
![PNG](src/05生活超市.png)  

## 05大型商场    
![PNG](src/06大型商场01.png)  
![PNG](src/06大型商场02.png)  
![PNG](src/06大型商场03.png)  
![PNG](src/06大型商场04.png)  

## 06公寓游泳池    
![PNG](src/07公寓游泳池.png)  

## 07公寓环境    
![PNG](src/08公寓环境.png)  

## 08单人公寓    
![PNG](src/09单人公寓.png)  
![PNG](src/09单人公寓2.png)  
![PNG](src/09单人公寓3.png)  
